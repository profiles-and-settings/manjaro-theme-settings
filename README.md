# manjaro-theme-settings

Shared theming settings for manjaro editions

- .gtk2rc
- .config/gtk-3.0/
- .config/qt5ct/
- .config/Trolltech.conf
- .config/Kvantum